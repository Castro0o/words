#!/bin/sh
currentpath=`pwd`
srate=44100

echo "\n\033[0;34m ------------ jackd -----------\033[00m\n"
jackd -d alsa -r $srate -p 1024 -n 2 -C hw:Pro,1 -P hw:Pro,0 -S &  # initiate jack server with fast_track sound card
#jackd -d alsa -r $srate -p 1024 -n 2 -D -C hw:0,0 -P hw:0,0 -S &
sleep 2

echo "\n\033[0;34m ------------ scsynth -----------\033[00m\n"
scsynth -u 57110 -S $srate & #start scsytnh
sleep 2

# No longer need as python connects to MIDI controller without using jackd
# # echo "\n\033[0;34m ------------ MIDI Connections -----------\033[00m\n"
# # aconnect -x  # remove all midi connections
# # aconnect 'nanoKONTROL':0 'SuperCollider':0 # connect midi  # list connection: aconnect -lio
# # sleep 2

echo "\n\033[0;34m ------------ sclang -----------\033[00m\n"
sclang $currentpath/boot.sc -u 57120 & #sclang # interpret SC script
sleep 4


echo "\n\033[0;34m ------------ Audio Connections -----------\033[00m\n"
jack_connect SuperCollider:out_1 system:playback_1 # connect SC to output
jack_connect SuperCollider:out_2 system:playback_2 # connect SC to output
jack_connect system:capture_1 SuperCollider:in_1 # connect SC to input
jack_connect system:capture_2 SuperCollider:in_2  # connect SC to input

# sleep 1

echo "\n\033[0;34m ------------ sc controls (python) -----------\033[00m\n"
python $currentpath/MIDI-nanoKONTROL.py 

#echo "\n\033[0;34m ------------ ENDED audio boot sequence -----------\033[00m\n"

#
