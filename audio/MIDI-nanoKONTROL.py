#!/usr/bin/env python

#########
# Script receives Midi messages from nanoKONTROL MIDI 1, processes and sends them to scsynth via OSC using osc.py
#########

import mido
from osc import *

# OSC
print msg
print client

def osc_msg(msg_list, osc_msg=msg, osc_client=client):
    osc_msg.extend(msg_list) 
    osc_client.send(osc_msg)
    print msg_list
    print osc_msg
    osc_msg.clearData()

# MIDI
inport = mido.open_input('nanoKONTROL MIDI 1')
    
# Synth Node Numbers
# * record - 1010 + synth_n 
# * player - 1020 + synth_n
# * rev - 1030 

# LOOP:
# Maps Midi controls and receives its messages
# Forwards them as OSC messages

for midi_msg in inport:
    # Faders
    if midi_msg.control >= 2 and midi_msg.control <= 13 :
        if midi_msg.control <= 6:
            control_index = midi_msg.control - 2
            if control_index <= 4: #Player vol
                synth_n = 1020 + control_index 
                osc_msg( [ "/n_set", "player", synth_n,
                           "vol", (float(midi_msg.value)/127)*2 ] )                            
        elif midi_msg.control >=8 and midi_msg.control <= 9:
            control_index = midi_msg.control - 3
            if control_index == 5: # Reverb mix
                synth_n = 1030
                osc_msg([ "/n_set", "reverb", synth_n,
                          'mix', (float(midi_msg.value)/127) ])
            
        elif midi_msg.control >=12 and midi_msg.control <= 13:
            control_index = midi_msg.control - 5            
        #print 'Fader' , control_index, midi_msg.control
        
    # --------> Knobs
    elif midi_msg.control >= 14 and midi_msg.control <= 22:
        control_index = midi_msg.control - 14
        if control_index <= 4:  # Player rate
            synth_n = 1020 + control_index 
            osc_msg( [ "/n_set", "player", synth_n,
                       "rate", (float(midi_msg.value)/float(63.5)) ] )
        elif control_index == 5: # Reverb damp            
            synth_n = 1030
            osc_msg([ "/n_set", "reverb", synth_n,
                       'damp', (float(midi_msg.value)/127)
                    ])
            
    # --------> Buttons Top    
    elif midi_msg.control >= 23 and midi_msg.control <= 31:
        control_index = midi_msg.control - 23
        if control_index <= 4: # Record midi_msg
            synth_n = 1010 + control_index
            if midi_msg.value == 127:
                osc_msg([ "/s_new", "recBuf", synth_n,
                          "bufnum", control_index ] )
            elif midi_msg.value == 0:            
                osc_msg([ "/n_free", "recBuf", synth_n])

    # --------> Buttons Bottom
    elif midi_msg.control >= 33 and midi_msg.control <= 41:
        control_index = midi_msg.control - 33
        if control_index <= 4:
            if midi_msg.value == 127:
                osc_msg([ "/b_free", control_index])
            # elif midi_msg.value == 0:            
            #     print 'button Bottom OFF', control_index            
    # --------> Transport 
    elif midi_msg.control >= 47 and midi_msg.control <= 49 :
        control_index = midi_msg.control - 47                   
        if midi_msg.value == 127:            
            print 'Transport ON', midi_msg.value, control_index
        elif midi_msg.value == 0:
            print 'Transport OFF', midi_msg.value, control_index

            
