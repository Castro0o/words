*In the end was the Word, and the Word was with Noise, and the Word was Noise. The same was in the end with Noise. All things were made by pulses; and without pulses was not any thing made that was made.*

The code for this project is comprised of several parts - audio, text, camera - each responsible for a part of the performance.
The parts don't communicate to eachother through code, but though the ouput from the printer.

# Audio
Sampling and reverb Fx.
* start/end scripts - initial audio scripts:
   * `audio_end.sh`
   * `audio_start.sh`
   * `boot.sc` - SC boot sequence
* control scripts:
   * `MIDI-nanoKONTROL.py` 
   * `osc.py`
   * `osc-receiver.sc`
* audio script: 
loop-synths.sc




# Text / Printer
Files:
* keybaord interaction / printing: invoke.py
* processes: anagrams.py markov.py rotation.py



# To Do
* narrow print area    
* camera:30
* play

0, u'one')Traceback (most recent call last):
File "./invoke.py", line 238, in <module>current_txt, current_list = process_input(func)
File "./invoke.py", line 68, in process_input
return current_txt, current_list
UnboundLocalError: local variable 'current_txt' referenced before assignment


## printer
* cancel print jobs
* use escape codes and empty lines for different operation: eg select: large type with large padding; rotation and permutation small padding.
* how to make the printer do more types of sounds and rhythmic sequences

## form/sound
* will come from: 
   * word delimiters: "\\n", " ", "     ",  "" 
   * line spacing
   * style formatting: font, font-size, bold, italics, text alignment

**Where to perform these operations?**
After current_txt is produced. Replacing their default delimiters by the chosen ones.

Appending esc/cos codes to the sentences

**How to intrage these into word generation?**
* depending on process that generates it
* random choice
E.g. Markov - large font, long space. Permutations: " " or "" as delimiters, Line spacing small. Style: italics or bold. Alignment: left, center, right. Font-size small or medium.

### Word delimiters
* orginal: @@
* replaced by: "\n", " ", "     ",  "" 


Complex line breaks:
* pyramid: 1 more word to each break
* square: fixed lenght of words
* wide narrot

## Markov
* prevent the result from markov.py from being only \n
* integrate full Gospel of John



# Sound
* samples: with volume and rate control
* reverb:
*

## routing audio (possibilities):
* from samples FULL - Post fader signal is sent to Reverb Fx; Rev volume determines how much is sent

* reverb is a mixer channel
   * aux N determines how much signal goes to reverb (Pre OR Post). Samples' output can also be sent to rev
   * mono input, stereo outout
   * (this way live signal can also have rev)
   

        IN audio1 ---> samples (recording) ---> samples player -- OUT audio 1,2
        
        IN audio2 ---> Reverb FX  -->  OUT audio 2,3
                       other FX (using same audio chain, but controlling drywet from that effect (see note)

How to use one channel for several FX

        IN --> ---- FX { dry/wet }  ---> FX2 { dry/wet } ---> OUT

SC has a dry/wet Ugen?
