#!/usr/bin/env python
from escpos.printer import Usb
from random import choice
lx300 = Usb(0x067b,0x2305,0)

text_format_dict = {'align':['LEFT', 'CENTER', 'RIGHT'],
                    'font':['A','B'],
                    'text_type': ['NORMAL','B', 'U', 'BU'],
                    'width': range(1,8),
                    'height':range(1,8),
                    'density':range(1,8),
                    'smooth': [True, False]
}

# lx300.control('LN') # line feed
# ???? where are B  or !b B at the start of the printing line coming from?
# margins ???

def to_printer(printer, content, formatting ):
    printer.hw('INIT') #initiate # removing previous formatting
    print formatting
    printer.line_spacing(choice(range(20,100)))
    printer.set(**formatting)
    printer.text(content)

t = "Naive linter for English prose for developers who can't write good and wanna learn to do other stuff good too."

text_format_choices = {key:choice(text_format_dict[key]) for key in text_format_dict.keys()} # creates a dict with random.choice for each of the text_format_dict values
to_printer( printer=lx300, content=t, formatting=text_format_choices)

