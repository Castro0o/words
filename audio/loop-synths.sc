// LOOP STATION 2016


//Loop Synths
/*
~delBuf = Buffer.alloc(s,44100*2,1); //Delay Max 2seg!

SynthDef(\delay, { |mul=0, deltime=0|
var input, delay;
	input = AudioIn.ar(1);
	delay = BufDelayL.ar(~delBuf, input, deltime, mul);
	Out.ar(0, Pan2.ar(delay,0,1))}).send(s);
*/

(
n=5;
~recbuffers = Array.new(n); n.do{ ~recbuffers.add(Buffer.alloc(s, 44100, 1)) };

SynthDef(\recBuf, {|in=1, bufnum, preRec|
var input, rec;
	input = AudioIn.ar(in);
	rec = RecordBuf.ar(input, bufnum, 0, 1, preRec);
	}).send(s);

/*
~rec = Synth(\recBuf,[\bufnum, ~recbuffers[0].bufnum]);
~rec.free;
~recbuffers[0].plot;
*/


SynthDef(\player, { |bufnum=0, rate=1, trg=1, startPos=0, loop=1, pan_pos=0, vol=0, out=0 |
	var play = PlayBuf.ar(1, bufnum, rate, trg, BufFrames.kr(bufnum)*startPos, loop);
	// var rand_trig = LFNoise0.kr(0.1).range(0.1,1);
	// var rand_pos = LFNoise1.kr( rand_trig ).range(-1,1);
	Out.ar(out, play*vol )// Pan2.ar(play, rand_pos, vol) )
}).send(s);

//MIDI ctl:rate and vol
//Random: pan_pos
//Out: 0,1

/*
~player=Synth(\player);
~player.set('bufnum', b.bufnum, 'vol',1, 'out',0);
~player.set('rate',0.1)
~player.free;
*/

// FX synths: \reverb, ?? AM ?? Distortion ??

SynthDef(\reverb, { |inchn=2, mix=0.0, room=0.6, damp=0.3, mul=0.5, out=1|
	var input = AudioIn.ar(inchn);
	var rev = FreeVerb.ar(input, mix, room, damp, mul);
	Out.ar(out, rev) }).send(s);
// MIDI ctl: mix 0..1 (dry/wet) --> fader;  damp 0..1 --> knwob

/*
~reverb = Synth(\reverb);
~reverb.set('inchn',2);
~reverb.set('mix',1);
~reverb.set('room',0.8);

*/


)

//o.remove;
//s.freeAll;
//s.sendMsg("/n_set", 1010, 'vol', 2, 'rate', 0.3);
