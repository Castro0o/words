s = Server.local;

s.waitForBoot {
	"hello".postln;

	loadRelative("loop-synths.sc");

// Wait for all asynchronous operations to complete, i.e. for the SynthDef to reach the server.
	s.sync;

		loadRelative("osc-receiver.sc");

	//thisProcess.interpreter.executeFile(~scmidi);


}