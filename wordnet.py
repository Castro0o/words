#!/usr/bin/env python

from nltk.corpus import wordnet as wn

def synsets(term):
    return wn.synsets( term )


#### Synonyms #####
def synonyms( term ):
    # produces synosyms of a word.
    # return 1 lists (without repeating words):
    #  synonym_objs: wordnet synets object (can be further user)
    # , synonym_strs: simply strings with the names
    wn_synonyms = synsets(term)
    synonym_strs = [] # just string
    for i in wn_synonyms :
        name_str = (i.name())[:-5]
        if name_str not in synonym_strs:# synonym_strs:
            synonym_strs.append(name_str)
            yield  name_str #synonym_strs

# synonyms = list(synonyms('meaaoo'))
# print 'synonym:',  synonyms


##### Definition of a word #####
def definition( term ):
    wn_synonyms = synsets(term)
    if len(wn_synonyms) > 0: #prevent empty results
        term_obj = ( synsets(term)[0] )
        definition = term_obj.definition()
        yield definition

    #    yield definition

# definition_str = list(definition('wtr'))
# print 'Definition:', definition_str

##### Is the term an English word? #####
def english_word( term ):
    if wn.synsets(term):
        return term

# english_words = []

# for term in ['dog', 'god', 'ogd', 'dgo']:
#     if english_word(term):
#        english_words.append(term) 
# print 'English_words:', english_words

# english = english_word('ogod')
# print english
