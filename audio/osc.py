import OSC, sys
from time import sleep
#OSC.OSCClient.connect('127.0.0.1', '57120')
client = OSC.OSCClient()
address = '127.0.0.1', 57120
client.connect( address )     # set the address for all following messages
print client

# OSC msgs are sent to create the new synths (from sc synthdefs)
# OSC msgs are sent to set the synths (via MIDI in MIDI-Nanocontrol)

msg = OSC.OSCMessage('loops') # OSCresponder name: '/loops'

# Players synths: add to server
for i in range(5): # 1006..1011
    synth_n = 1020+i
    msg.extend( [ "/s_new", "player", synth_n, 'bufnum', i, 'vol',0  ])    
    print msg
    client.send(msg)
    msg.clearData()
    sleep(float(0.1))

# Reverb synth: add to server
msg.extend( [ "/s_new", "reverb", 1030,  'mix', 0.5 ]) # cmd, synth, synthId
print msg
client.send(msg)
sleep(float(0.1))
# msg.clearData()
# sleep(1)
# msg.extend( [ "/n_set", "reverb", 1005, 'mix', 1 ]) # bufnum will be item from BUFFER LIST    
# print msg
# client.send(msg)
# msg.clearData()


#sys.exit("some error message")
#sys.exit("client.close()")
    
# client.close()

