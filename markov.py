#!/usr/bin/env python

# Markov Chains

# To generate a Markov cahin, program needs to find out all the transitions for all words
# And the frequency in which they occur
# Based on transition matrix (weights) do random choice: numpy.choice(elements, wheights)

from pprint import pprint
from numpy import matrix
from numpy.random import choice
import random


john_file = open('John.txt','r')
john=john_file.read()
'''In the beginning was the Word, and the Word was with God, and the Word was God.
The same was in the beginning with God.
All things were made by him; and without him was not any thing made that was made.
In him was life; and the life was the light of men.'''

john_lines = john.split('\r')




def process_txt(txt): # process txt (rm pontuaction, lower case), return txt as list of word
    txt = txt.lower()
    txt = txt.replace('.','').replace(',','').replace(';','').replace(':','').replace('!','').replace('?','').replace('(','').replace(')','')
#    txt = txt.replace('\n', ' OOO ') #so \n becomes item from words list 
    words = txt.split(' ')
    return words

lines_words = []
for line in john_lines:
    words = process_txt(line)
    lines_words.append(words)

#print lines_words
    



# What are the transitions in text: word:[ next_word1, next_word2]
# input arg: [ [sentence, word, by, word], [another, sentence, word, by, word]]
# i.e. 'in': ['the', 'the', 'him']
def transition(lines_words):
    dict_trans = {}    
    for line in lines_words:
        line_len = len(line)        
        for i, current_w in enumerate(line):        
            if i == (line_len-1):
                next_w = '\n'
            else:
                next_w = line[i+1]
            if current_w not in dict_trans.keys():
                dict_trans[current_w] = [next_w]
            else:
                dict_trans[current_w].append(next_w)
    # pprint( dict_trans )
    # print dict_trans.keys()
    return dict_trans





# what are the possibilities and probabilities for the transition
# returns dictionary with word: [freq of transitions]
# i.e. 'in': [('the', 0.66666667), ('him', 0.33333333)],

def probability(translist): 
    past_i = [] #to prevent repetition
    trans_probability = []
    for i in translist:
        if i not in past_i:
            past_i.append(i)
            freq = round((float(translist.count(i)) / float(len(translist))),8)
            trans_probability.append( (i, freq) )
    return trans_probability

def transition_occurances(dict_trans): # create the transition matrix
    keys = dict_trans.keys() # keys is an alphatically order list of all the word in text
    keys.sort() 
    pre_matrix = []
    matrix = {}
    for key in keys: # each key will result in a list of probabilities, the size of number of words
        list_weights = [0 for i in range( len(keys) )] # start with a list of zeros
        # get transitions probabilities
        probs = probability(dict_trans[key])
        # print key, probs    
        matrix[key] = probs # replace possible following word w/ tuples (word, prob),(word,prob)
    return matrix

dict_trans = transition(lines_words)
dict_trans.pop('\n', None) # remove \n keys
dict_trans_occur = transition_occurances(dict_trans)
#pprint(dict_trans_occur) #e.g. 'him': [('and', 0.3333333333333333), ('was', 0.6666666666666666)],

# dict_trans_occur SHOULD ONLY BE GENERATED ONCE


# # Generate sentence

#old stable
def markov_next_w(current_w, result): 
    row = dict_trans_occur[current_w]
    terms = [i[0] for i in row]
    weights = [i[1] for i in row]
    next_w = choice(terms, p=weights)
    # TODO: prevent the result from being only \n
    if next_w == '\n':
        return result + next_w
    else:
        return current_w + ' '+ markov_next_w(next_w, result)


###

# def invoke_markov(inpt):
# #    pprint( dict_trans_occur)
#     keys = dict_trans_occur.keys()
# #    print keys
#     if inpt not in keys:
#         inpt = random.choice(keys)
#     markov = markov_next_w(inpt) # markov is list
#     return markov

# m = invoke_markov('god')
# print m
# here is where we need to interveen in order to prevent empty sentences

