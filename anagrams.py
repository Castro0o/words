#!/usr/bin/env python
    
def anagrams(inpt):
    if " " in inpt: # word or sentence
        elements = inpt.split(' ') 
    else:
        elements = inpt #list of letter, which make the word

    if len(elements) <=1:
        yield elements
    else:
        for perm in anagrams(elements[1:]): #for every element after 1st element of elements execute the function
            #print 'perm', perm
            #print 'len',  len(elements)
            for i in range(len(elements)): #for size of elements
                #print 'i', i
                yield perm[:i] + elements[0:1] + perm[i:] #

# word = "cola"
# print list(anagrams(word))
# for i in anagrams(word):
#     print i
