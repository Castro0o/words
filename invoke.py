#!/usr/bin/env python
import os
from curses_prototype import *
from markov import * 
from anagrams import *
from rotation import *
from wordnet import *

if os.path.exists('/dev/usb/lp2'): 
    from printer import *
    printing=True
else :
    printing=False
from time import sleep


# ## Script to invoke word generation and processing ##
# User input: curses
#
# # Operations description #
# * curses loop: expecting keyboard input
# * once a key is pressed it goes on to corresponding condition
# * calling the func
#   * func from "Generators & Text processors"
#   * these defs call functions in external scripts (anagrams, markov_next_w)
# * func =  innoke_ProcessName
# * process_input receives function and return current_txt, current_list (global vars) and adds them to screen
# * senteces are treated as lists: single items or all items are select (numbers) and are asigned to current_txt, current_list
#
# # Bugs #
#
#

def search_sublist(parent_list):
    ''' a bit fidly: looks only at first element to determine if has list or not'''    
    for el in parent_list:
        if type(el) is list:
            return True
        else:
            return False

        
def process_input(inpt):
    '''    Receives product from generator/processors, often in form of list or string;
    determines what input it receives;
    outputs: current_txt, current_list;        
    '''
    
    screen.addstr(3,0, 'INPUT: '+str(inpt) )    
    if type(inpt) is list:
        sublist_bool = search_sublist(inpt)
        if sublist_bool is True: # list of list
            current_list = []
            for sublist in inpt:
                sublist =  " ".join(sublist)#or \n
                current_list.append(sublist)
            current_txt = " ".join( current_list)
            current_list =  [(n, word) for n, word in enumerate(current_list)]
        else:
            current_txt = " ".join(inpt)#or " \"
            current_list =  [(n, word) for n, word in enumerate(inpt)]
    elif type(inpt) is str:
        if " " in inpt: # if inpt is a sentence
            inpt_list = inpt.split(" ")
        else:
            inpt_list = [inpt]
        current_list =  [(n, word) for n, word in enumerate(inpt_list)]
        current_txt = inpt
    return current_txt, current_list


def delimiter(inpt_txt, func_name):
    '''replaces default delimiter @@ with another'''
    delimiter_dic = { 'invoke_markov': [' ', '   ', '\n'],
                      'invoke_permutation': [ '', ' ', '   ', '\n'],
                      'invoke_rotation': [ '', ' ', '   ', '\n'],
                      'invoke_synonyms': [ '', ' ', '   ', '\n'],
                      'invoke_definition': [ '', ' ', '   ', '\n']
        }
    random_delimiter = random.choice( (delimiter_dic[func_name]))    
    out_txt = inpt_txt.replace(" ", random_delimiter)
    return out_txt #out_txt

def printer_format(inpt_txt,func_name ):
    '''adds escpos codes to the inpt_txt and resets at the end'''
    formatting_dic = {
        'invoke_markov': {
            'justify': ['justify_center', 'justify_left'],
            'emphasis': ['double_on', 'emphasis_on'],
            'fontsize': ['largefont', 'mediumfont']},

        'invoke_rotation': {
            'justify': ['justify_center', 'justify_left', 'justify_right'],
            'emphasis': ['emphasis_on','emphasis_off'],
            'fontsize': ['normalfont', 'mediumfont']
        },
        
        'invoke_permutation': {
            'justify': ['justify_center', 'justify_left'],
            'emphasis': ['emphasis_on','emphasis_off'],
            'fontsize': ['normalfont']
        },
        
        'invoke_select': {
            'justify': ['justify_center', 'justify_left'],
            'emphasis': ['double_on', 'emphasis_on'],
            'fontsize': ['largefont', 'mediumfont']
        },
        
        'invoke_synonyms': {
            'justify': ['justify_center', 'justify_left'],
            'emphasis': ['double_on', 'emphasis_on'],
            'fontsize': ['largefont', 'mediumfont']},

        'invoke_definition': {
            'justify': ['justify_center', 'justify_left'],
            'emphasis': ['double_on', 'emphasis_on'],
            'fontsize': ['largefont', 'mediumfont']}

    }
    formatting_choices = [choice(formatting_dic[func_name][key]) for key in formatting_dic[func_name]]
    formatting_choices_escpos = [escpos[item] for item in formatting_choices] 
    formatting_str = " ".join(formatting_choices_escpos) #as tring
    out_txt =  escpos["init_printer"] + formatting_str + inpt_txt 
    return out_txt


def check_len(process, current_def_txt, current_def_list):
    '''check if the len of input is small enough to process'''    
    len_max = { "invoke_permutation":5, 
                "invoke_rotation":10
            }    
    if len(current_def_list) <= len_max[process.func_name]:
        func_output  = process(current_def_txt)
        current_def_txt, current_def_list = process_input(func_output)
        screen.addstr(12,0, str(current_def_list))
    else:
        screen.addstr(4,0, 'Input too large', curses.A_UNDERLINE)
        screen.addstr(5,0, 'Choose 1 item', curses.A_UNDERLINE)
        screen.addstr(12,0, str(current_def_list))
    return current_def_txt, current_def_list

# Generators & Text processors 

# def invoke_markov(inpt):
# #    pprint( dict_trans_occur)
#     keys = dict_trans_occur.keys()
# #    print keys
#     if inpt not in keys: # if cannot start sentnce with inpt, because inpt is not in keys
#         inpt = random.choice(keys) # choose random word
#     screen.addstr(4,0, str(inpt), curses.A_UNDERLINE)        
#     markov = markov_next_w(inpt) # markov is list
#     return markov

def invoke_markov(inpt):
    keys = dict_trans_occur.keys()
    if inpt not in keys:
        inpt = random.choice(keys)
    screen.addstr(4,0, str(inpt), curses.A_UNDERLINE)        
    markov = markov_next_w(inpt, '') # markov is list
    return markov

def invoke_permutation(inpt):
    permutation_output = anagrams(inpt)
    permutation_output = list(permutation_output)
    return permutation_output

def invoke_rotation(inpt):
    rotatio_output = rotation(inpt)
    rotatio_output = list(rotatio_output)
    return rotatio_output

def invoke_synonyms(inpt):
    synonyms_output = synonyms(inpt)
    synonyms_output = list(synonyms_output)
    return synonyms_output

def invoke_definition(inpt):
    definition_output = definition(inpt) # string
    definition_output = list(definition_output)#.split(' ')
#    definition_output = (list(definition_output)[0]).split(' ')
    return definition_output


# def reverse(txt):
#     l = list(txt)
#     l.reverse()
#     r = ''.join(l)
#     return r

# def upper(txt):
#     up=txt.upper()
#     return up        
                

#Curses
def curses_current_menu(process):
    screen.clear()
    screen.addstr(process, curses.A_REVERSE)
    screen_menu()
    screen_help()


screen_menu()
screen_help()
current_txt = ''
current_list = []

while True:
    '''Curses Loop'''
    screen.scrollok(1)
    event = screen.getch()
    key= None
    if event < 256:
        key = chr(event)
    
    # Sytem commands
    if event == ord("Q"):
        curses.endwin()
        break

    elif event == ord("T"):
        screen.clear()
        screen.addstr("TEST", curses.A_STANDOUT)
        screen.addstr(str(chr(event)))
        screen_help()

    # elif event == ord("M"):
    #     screen.clear()
    #     screen_menu()
    #     screen_help()        

    # generators and processes
    
    elif event == ord("m"):
        process = "invoke_markov"
        curses_current_menu(process) 
        func  = invoke_markov(current_txt)
        current_txt, current_list = process_input(func)
        screen.addstr(6,0, str(current_txt))
        screen.addstr(12,0, str(current_list))
        if printing is True and len(current_list) and current_txt != '\n': 
            printer_txt = delimiter(current_txt, process)
            printer_txt = printer_format(printer_txt, process)
            to_printer(current_txt)
        
    elif event == ord("p"):
        # \n needs to be removed ?
        process = "invoke_permutation"
        curses_current_menu(process) 
        current_txt, current_list = check_len(invoke_permutation, current_txt, current_list)
        if printing is True and len(current_list) and current_txt != '\n':
            printer_txt = delimiter(current_txt, process)
            printer_txt = printer_format(printer_txt, process)
            to_printer(current_txt)


    elif event == ord("r"):
        process = "invoke_rotation"
        curses_current_menu(process) 
        current_txt, current_list = check_len(invoke_rotation, current_txt, current_list)
        if printing is True and len(current_list) and current_txt != '\n': 
            printer_txt = delimiter(current_txt, process)
            printer_txt = printer_format(printer_txt, process)
            to_printer(current_txt)


    elif event == ord("s"): # synonyms s
        process = "invoke_synonyms"
        func  = invoke_synonyms(current_txt)
        current_txt, current_list = process_input(func)
        screen.addstr(6,0, str(current_txt))
        screen.addstr(12,0, str(current_list))
        if printing is True and len(current_list) and current_txt != '\n': 
            printer_txt = delimiter(current_txt, process)
            printer_txt = printer_format(printer_txt, process)
            to_printer(current_txt)
            
    elif event == ord("d"): # definition d
        process = "invoke_definition"
        func  = invoke_definition(current_txt)
        current_txt, current_list = process_input(func)
        screen.addstr(6,0, str(current_txt))
        screen.addstr(12,0, str(current_list))
        if printing is True and len(current_list) and current_txt != '\n': 
            printer_txt = delimiter(current_txt, process)
            printer_txt = printer_format(printer_txt, process)
            to_printer(current_txt)



            
    # choose 1 word from list of words
    elif event in ([ord(str(i)) for i in range(10)]):
        if len(current_list) == 0:
            screen.clear()
            screen_menu()
            screen_help()
            screen.addstr(6,0, 'Empty list', curses.A_REVERSE)
        elif  len(current_list) > 0:
            process = "invoke_select"
            current_list = current_list[int(key)]
            current_txt = current_list[1]
            curses_current_menu("Select N")
            screen.addstr(5,0, str(current_list))
            screen.addstr(6,0, str(current_txt))

            if printing is True and len(current_list) and current_txt != '\n': 
                printer_txt = printer_format(printer_txt, process)
                to_printer(current_txt)

    # choose whole sentence
    elif event is ord("a"):
        if len(current_list) == 0:
            screen.clear()
            screen_menu()
            screen_help()
            screen.addstr(6,0, 'Empty list', curses.A_REVERSE)
        elif len(current_list) > 0:
            current_txt, current_list = process_input(current_txt)
            curses_current_menu("Select All")
            screen.addstr(9,0, str(current_list))
            
    elif event is ord("\n"): # return key        
        to_printer('\n')














