// OSCresponder for LOOP STATION 2016

( // OSCresponderNode  listening to any client: nil
o = OSCresponderNode(nil, '/loops', { |t, r, msg|
	var cmd = msg[1];
	var synthName = msg[2]; // also used with buffer index in b_free
	var synthId = msg[3];
	var synthArgs = (msg.copyToEnd(4)).clump(2);
	[cmd, synthName, synthId, synthArgs].postln;

	// todo: add synthArgs recursively, in response to size

	if (cmd == "/s_new".asSymbol){
		if( (synthArgs.size > 0),
			{ if( (synthArgs[0][0] == "bufnum" ),
				{s.sendMsg(cmd, synthName, synthId, 1, 1, synthArgs[0][0], ~recbuffers[synthArgs[0][1]]); },
				{s.sendMsg(cmd, synthName, synthId, 1, 1, synthArgs[0][0], synthArgs[0][1])}
				)}, {s.sendMsg(cmd, synthName, synthId, 1, 1);});
		'new'.postln; };

	if (cmd == "/n_set".asSymbol){ synthArgs.size.do{|i|
		s.sendMsg(cmd, synthId,	synthArgs[i][0], synthArgs[i][1]);
		'set'.postln; } };

	if (cmd == "/n_free".asSymbol){  s.sendMsg(cmd, synthId); 'free'.postln;};

	if (cmd == "/b_free".asSymbol){~recbuffers[synthName].zero; ('buffer zeroed'+synthName).postln;}; //free buffer

});
o.add;
)

//o.remove;
//s.freeAll;
//s.sendMsg("/n_set", 1010, 'vol', 2, 'rate', 0.3);
